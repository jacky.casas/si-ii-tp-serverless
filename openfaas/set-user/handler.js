var MongoClient = require('mongodb').MongoClient;

module.exports = (event, context) => {
    var url = 'VOTRE_CONNECTION_STRING_MONGODB';
    MongoClient.connect(url, function (err, client) {
        var db = client.db('test');

        var response = {}; 
        if (err || !('body' in event) || !('user' in event.body)) {
            response = {
                status: 'Error',
                err: err
            }; 
        } else {
            db.collection('users').insertOne({
                first_name: event.body.user.first_name,
                last_name: event.body.user.last_name,
                username: event.body.user.username,
                telegram_id: event.body.user.id,
                type: event.body.user.type,
                time: new Date()
            });
            response = {
                status: 'User correctly subscribed',
                event: event
            };
        }
        client.close();
        
        context
            .status(200)
            .succeed(response);
    });
};