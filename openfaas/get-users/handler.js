var MongoClient = require('mongodb').MongoClient;

module.exports = (event, context) => {
    let err;
    var url = 'VOTRE_CONNECTION_STRING_MONGODB';
    MongoClient.connect(url, function (err, client) {
        var db = client.db('test');
        db.collection('users').find({}).toArray(function(err, result) {
            if (err) throw err;
            var data = [];
            for (var i = 0; i < result.length; i++) {
                data.push(result[i]);
            }
            client.close();
    
            var response = { users: data, status: 'ok'};
            context
                .status(200)
                .succeed(response);
        });
    });
};
